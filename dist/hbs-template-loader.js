"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var chalk = require("chalk");
var glob = require("glob");
var handlebars = require("handlebars");
var beautify = require("js-beautify");
var loaderUtils = require("loader-utils");
var __HBS__ = handlebars.create();
var __PARTIALS__ = [];
var __HELPERS__ = [];
var __CONTEXT__ = [];
var __CONTEXT_DATA__ = {};
function hbsLoader(source) {
    var _this = this;
    var chalkCtx = chalk.constructor();
    var consolePrefix = chalkCtx.green("hbs-loader:");
    var defaults = {
        app: '',
        helpers: [],
        partials: [],
        context: [],
        charset: 'utf8',
        htmlBeautifyOptions: {}
    };
    var options = loaderUtils.getOptions(this);
    var config = __assign({}, defaults, options);
    var template;
    var html;
    var beautifiedHtml;
    if (this.cacheable) {
        this.cacheable(true);
    }
    config.context.forEach(function (contextGlob) {
        var contextFiles = glob.sync(contextGlob);
        contextFiles.forEach(function (filePath) {
            var contextName = path.basename(filePath, '.json');
            var jsonString = fs.readFileSync(filePath, 'utf8');
            var jsonObj;
            if (__CONTEXT__.indexOf(contextName) < 0) {
                try {
                    jsonObj = JSON.parse(jsonString);
                    console.log(consolePrefix + " Handlebars context '" + contextName + "' added...");
                }
                catch (e) {
                    console.log(consolePrefix + " File '" + filePath + "' could not be parsed as JSON!");
                    throw e;
                }
                _this.addDependency(filePath);
                __CONTEXT__.push(contextName);
                __CONTEXT_DATA__[contextName] = __assign({}, jsonObj);
            }
        });
    });
    config.helpers.forEach(function (helper) {
        var helperName = helper.name;
        if (__HELPERS__.indexOf(helperName) < 0) {
            __HBS__.registerHelper(helper(__HBS__));
            __HELPERS__.push(helperName);
            console.log(consolePrefix + " Handlebars helper '" + helperName + "' registered...");
        }
    });
    config.partials.forEach(function (partial) {
        var partialFiles = glob.sync(partial);
        partialFiles.forEach(function (filePath) {
            var partialName = path.basename(filePath, '.hbs');
            if (__PARTIALS__.indexOf(partialName) === -1) {
                __HBS__.registerPartial(partialName, fs.readFileSync(filePath, config.charset));
                console.log(consolePrefix + " Handlebars partial '" + partialName + "' registered...");
                __PARTIALS__.push(partialName);
                _this.addDependency(filePath);
            }
        });
    });
    template = __HBS__.compile(source);
    var app = this.context.split('app/').pop().split('/templates')[0];
    if (!config.app && app.length) {
        config.app = app;
    }
    if (!config.app) {
        console.log('App is not given. Please check');
    }
    html = template({ meta: __CONTEXT_DATA__[config.app] });
    beautifiedHtml = beautify.html(html, config.htmlBeautifyOptions);
    return beautifiedHtml;
}
exports.default = hbsLoader;
//# sourceMappingURL=hbs-template-loader.js.map