import * as fs from 'fs'
import * as path from 'path'

import * as chalk from 'chalk'
import * as glob from 'glob'
import * as handlebars from 'handlebars'
import * as beautify from 'js-beautify'
import * as loaderUtils from 'loader-utils'

interface IHbsTemplateLoaderOptions {
  app?: string,
  // An array of handlebars helper functions
  helpers?: any[],
  // A list of file globs
  partials?: string[],
  // A list of file globs
  context?: string[],
  charset?: string,
  htmlBeautifyOptions?: HTMLBeautifyOptions
}

// Shared Handlebars instance
const __HBS__: typeof handlebars = handlebars.create()
// Shared list of registered partials
const __PARTIALS__: string[] = []
// Shared list of registered helpers
const __HELPERS__: string[] = []
// Shared list of context JSON file paths
const __CONTEXT__: string[] = []
// Shared context/data object
const __CONTEXT_DATA__: object = {}

/**
 * Default loader function
 * @param source
 */
function hbsLoader(source: string): string {
  // Chalk instance
  const chalkCtx = chalk.constructor()
  // A console log prefix
  const consolePrefix: any = chalkCtx.green(`hbs-loader:`)
  // Default option values
  const defaults: IHbsTemplateLoaderOptions = {
    app: '',
    helpers: [],
    partials: [],
    context: [],
    charset: 'utf8',
    htmlBeautifyOptions: {}
  }
  // Options passed to loader
  const options: IHbsTemplateLoaderOptions = loaderUtils.getOptions(this)
  // Merged configuration object
  const config: IHbsTemplateLoaderOptions = { ...defaults, ...options }
  // Handlebars template
  let template: HandlebarsTemplateDelegate<any>
  // Resulting template html string
  let html: string
  // Beautified html string
  let beautifiedHtml: string

  // Cache results when possible
  if (this.cacheable) {
    this.cacheable(true)
  }

  // Create handlebars context object
  config.context.forEach((contextGlob: string) => {
    // Read each context string as a glob
    const contextFiles: string[] = glob.sync(contextGlob)

    contextFiles.forEach((filePath) => {
      const contextName: string = path.basename(filePath, '.json')
      const jsonString: string = fs.readFileSync(filePath, 'utf8')
      let jsonObj: JSON

      if (__CONTEXT__.indexOf(contextName) < 0) {
        // Attempt to parse JSON
        try {
          jsonObj = JSON.parse(jsonString)
          console.log(`${consolePrefix} Handlebars context '${contextName}' added...`)
        } catch (e) {
          console.log(`${consolePrefix} File '${filePath}' could not be parsed as JSON!`)
          throw e
        }

        // Add file as dependency in order to make them watchable
        this.addDependency(filePath)

        // Add file path to global list
        __CONTEXT__.push(contextName)

        // Merge into context object
        __CONTEXT_DATA__[contextName] = {...(jsonObj as {})}
      }
    })
  })

  // Register Handlebars helpers
  config.helpers.forEach((helper: any) => {
    const helperName: string = helper.name

    if (__HELPERS__.indexOf(helperName) < 0) {
      // Invoke each helper, passing our instance of handlebars
      __HBS__.registerHelper(helper(__HBS__))
      // Add helper name to global list
      __HELPERS__.push(helperName)
      console.log(`${consolePrefix} Handlebars helper '${helperName}' registered...`)
    }
  })

  // Register Handlebars partials
  config.partials.forEach((partial: string) => {
    // Read each pattern as glob which results in array of path strings
    const partialFiles: string[] = glob.sync(partial)

    partialFiles.forEach((filePath: string) => {
      const partialName = path.basename(filePath, '.hbs')

      if (__PARTIALS__.indexOf(partialName) === -1) {
        // Register handlebars partial using the file's basename as the partial name
        __HBS__.registerPartial(partialName, fs.readFileSync(filePath, config.charset))
        console.log(`${consolePrefix} Handlebars partial '${partialName}' registered...`)
        // Add partial name to global list
        __PARTIALS__.push(partialName)
        // Add file as dependency in order to make them watchable
        this.addDependency(filePath)
      }
    })
  })

  // Create Handlebars template
  template = __HBS__.compile(source)
  const app: string = this.context.split('app/').pop().split('/templates')[0];
  if (!config.app && app.length) {
    config.app = app;
  }
  if (!config.app) {
    console.log('App is not given. Please check');
  }
  // Invoke template
  html = template({ meta: __CONTEXT_DATA__[config.app]})
  // Beautify html
  beautifiedHtml = beautify.html(html, config.htmlBeautifyOptions)

  return beautifiedHtml
}

export default hbsLoader
