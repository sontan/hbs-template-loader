# Handlebars Template Loader

A [Handlebars](https://github.com/wycats/handlebars.js) template loader for [Webpack](https://github.com/webpack/webpack).

## NOTE
This is broken version. Don't use it.

## Install

```
npm install --save-dev hbs-template-loader
```

## Usage

This loader is intended to be used with `raw-loader` or `html-loader`.

```
// This import is shown here to demonstrate using a handlebars helper.
// It's not required to use the loader.
const hbsLayouts = require('handlebars-layouts');

...

{
  test: /\.(hbs|handelbars)$/,
  exclude: /node_modules/,
  use: [{
    loader: 'raw-loader'
  }, {
    loader: 'hbs-template-loader',
    options: {
      helpers: [
        hbsLayouts
      ],
      partials: [
        `${SRC_PATH}/templates/partials/**/*.hbs`
      ],
      context: [
        `${SRC_PATH}/templates/data/**/*.json`
      ],
      htmlBeautifyOptions: {
        indent_size: 2,
        indent_char: ' ',
        indent_with_tabs: false
      },
      charset: 'utf8'
    }
  }]
}

...

plugins: [
  new HtmlWebpackPlugin({
    template: `${SRC_PATH}/templates/views/index.hbs`,
    filename: 'index.html'
  }),
]
...
```

## Options

`options.helpers`

An array of Handlebars helper functions

`options.partials`

An array of Handlebars partial file glob strings

`options.context`

An array of json file glob strings that will be read and added to the Handlebars context object

`options.charset`

A charset used for filesystem read/write and Handlebars compilation

`options.htmlBeautifyOptions` 

JS-Beautify html options. See their [docs](https://github.com/beautify-web/js-beautify#css--html) for all options.

## Testing

Ummmm...this is awkward. So, the thing is, the tests don't exist yet. "Coming soon", as they say.